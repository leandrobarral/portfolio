import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import fontawesome from '@fortawesome/fontawesome';
import solid from '@fortawesome/fontawesome-free-solid';
import regular from '@fortawesome/fontawesome-free-regular';
import brands from '@fortawesome/fontawesome-free-brands';

fontawesome.library.add(solid, regular, brands);

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { WorkExperienceComponent } from './work-experience/work-experience.component';
import { ProjectOthersComponent } from './projects/project-others/project-others.component';
import { ProjectHighlightsComponent } from './projects/project-highlights/project-highlights.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutMeComponent,
    WorkExperienceComponent,
    ProjectOthersComponent,
    ProjectHighlightsComponent
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    AboutMeComponent,
    WorkExperienceComponent,
    ProjectOthersComponent,
    ProjectHighlightsComponent
  ]
})
export class AppModule {}
